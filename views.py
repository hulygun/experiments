# coding: utf-8
from httplib import BAD_REQUEST

from django.http import HttpResponse
from django.views.generic import View
from common.decorators import headers  # из основного проекта
from common.utils.json import JsonResponse  # из основного проекта

from .exceptions import WrongGoal


class ExperimentView(View):
    def get(self, request, *args, **kwargs):
        """Получение варианта по эксперименту и выбор нового"""
        experiment_name = request.GET.get('experiment')
        experiment = request.experiments(experiment_name)
        if not experiment:
            return json_error(u'Experiment "{}" not found'.format(experiment_name))

        variant = experiment.get_or_select_variant()
        return JsonResponse({
            'variant': variant.name,
            'selected': variant.selected,
            'is_default': variant.is_default
        })

    def post(self, request, *args, **kwargs):
        """Фиксирование достижения цели по эксперименту"""
        experiment_name = request.POST.get('experiment')
        goal = request.POST.get('goal')
        experiment = request.experiments(experiment_name)
        if not experiment:
            return json_error(u'Experiment "{}" not found'.format(experiment_name))

        try:
            goal_data = experiment.goal(goal)
        except WrongGoal as e:
            return json_error(unicode(e))

        return JsonResponse({
            'was_reached': goal_data.was_reached,
            'is_first_time': goal_data.is_first_time
        })


@headers({
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'X-Requested-With, Content-Type',
})
def confirm_human(request):
    """Подтверждение человекоподобности текущего участника эксперимента"""
    if request.is_ajax() and request.method == 'POST':
        request.participant.confirm_human()

    return HttpResponse()


def json_error(message, status_code=BAD_REQUEST):
    return JsonResponse({'errors': {'message': message}}, status=status_code)
