# coding: utf-8
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from .models import ExperimentModel, Variant
from .cache_helpers import get_all_experiments


@receiver(post_save, sender=ExperimentModel)
@receiver(post_delete, sender=ExperimentModel)
@receiver(post_save, sender=Variant)
@receiver(post_delete, sender=Variant)
def experiments_cache_clear(sender, instance, **kwargs):
    get_all_experiments.clear_cache()
