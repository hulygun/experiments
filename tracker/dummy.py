# coding: utf-8
from .base import BaseTracker


class DummyTracker(BaseTracker):
    def track_participant(self, experiment, variant):
        pass

    def track_goal(self, experiment, variant, goal):
        pass
