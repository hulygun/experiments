# coding: utf-8
import datetime
from django.utils.timezone import now

from ..statistics import ExperimentStat
from .base import BaseTracker


class RedisTracker(BaseTracker):
    """
    Трекер участия в эксперименте/достижения цели
    """
    PREFIX = 'experiments'
    STAT_PREFIX = 'experiment_stat'
    STAT_DATE_FORMAT = '%d-%m-%Y'

    def __init__(self, connection):
        self.connection = connection

    def track_participant(self, experiment, variant):
        """
        Фиксирование участия в эксперименте
        :param str experiment: эксперимент
        :param str variant:  вариант
        """
        self.connection.hincrby(
            name='{}:{}'.format(self.PREFIX, experiment),
            key=variant
        )

    def track_goal(self, experiment, variant, goal):
        """
        Фиксирование достижения цели
        :param str experiment: эксперимент
        :param str variant: вариант
        :param str goal: цель
        """
        self.connection.hincrby(
            name='{}:{}'.format(self.PREFIX, experiment),
            key='{}:{}'.format(variant, goal)
        )

    def get_experiment_stat(self, experiment, variants, goals):
        """
        Получение статистики эксперимента
        :param experiment: str эксперимент
        :param list[str] variants: список возможных вариантов
        :param list[str] goals: список доступных целей
        :rtype: experiments.statistics.ExperimentStat
        """
        raw_data = self.connection.hgetall('{}:{}'.format(self.PREFIX, experiment))
        get_int_value = lambda *args: int(raw_data.get(':'.join(args), 0))

        stat = ExperimentStat()
        for variant in variants:
            stat.set_variant_participants(variant, get_int_value(variant))
            for goal in goals:
                stat.set_goal_hits(variant, goal, get_int_value(variant, goal))

        return stat

    def track_daily_stats(self, experiment, variant, goal, conversion):
        self.connection.hset(
            name='{}:{}:{}:{}'.format(self.STAT_PREFIX, experiment, variant, goal),
            key=now().strftime(self.STAT_DATE_FORMAT),
            value=conversion
        )

    def get_experiment_daily_stat(self, experiment, variants, goals):
        """
        Получение ежедневной статистики эксперимента
        :param experiment: str эксперимент
        :param list[str] variants: список возможных вариантов
        :param list[str] goals: список доступных целей
        :rtype: dict[str, dict[datetime.date, dict[str, float]]]
        """
        stat = {}
        for goal in goals:
            stat[goal] = {}

            for variant in variants:
                key = '{}:{}:{}:{}'.format(self.STAT_PREFIX, experiment, variant, goal)
                for date, conversion in self.connection.hgetall(key).iteritems():
                    date = datetime.datetime.strptime(date, self.STAT_DATE_FORMAT).date()
                    stat[goal].setdefault(date, {})
                    stat[goal][date][variant] = float(conversion)

        return stat

    def get_experiment_conversion(self, experiment, variants, goals):
        output = dict()
        for variant in variants:
            output[variant] = {
                goal: self.connection.hgetall('experiment_stat:{}:{}:{}'.format(experiment, variant, goal))
                for goal in goals
            }
        return output
