# coding: utf-8
class BaseTracker(object):
    """
    Трекер участия в эксперименте/достижения цели
    """
    def track_participant(self, experiment, variant):
        """
        Фиксирование участия в эксперименте
        :param str experiment: эксперимент
        :param str variant:  вариант
        """
        raise NotImplementedError()

    def track_goal(self, experiment, variant, goal):
        """
        Фиксирование достижения цели
        :param str experiment: эксперимент
        :param str variant: вариант
        :param str goal: цель
        """
        raise NotImplementedError()
