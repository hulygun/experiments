# coding: utf-8
import json
import datetime
from collections import defaultdict

from django.contrib import admin
from django.conf import settings
from django.forms import BaseInlineFormSet, ValidationError
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe

from cache.decorators import memoized_in_object  # кеширование из основного проекта
from common.utils import admin_column  # из основного проекта
from core.utils import to_timestamp  # из основного проекта

from .models import ExperimentModel, Variant
from .cache_helpers import get_experiment_stat, get_experiment_daily_stat


class VariantInlineFormSet(BaseInlineFormSet):
    def clean(self):
        super(VariantInlineFormSet, self).clean()
        if not any(self.errors):
            is_default_count = len(filter(None, (
                form.cleaned_data['is_default']
                for form in self.forms
                if not form.cleaned_data.get('DELETE', True)
            )))

            if is_default_count != 1:
                raise ValidationError(u"Нужно выбрать один вариант по-умолчанию")


class VariantInline(admin.TabularInline):
    model = Variant
    extra = 2
    fields = ('name', 'set_as_experiment_variant', 'is_default',)
    readonly_fields = ('set_as_experiment_variant',)
    formset = VariantInlineFormSet

    def set_as_experiment_variant(self, obj):
        if obj.pk:
            link_tpl = '<a href="/?set_variant={}:{}" target="_blank">Установить как вариант для эксперимента</a>'
            return mark_safe(link_tpl.format(obj.experiment.name, obj.name))
        else:
            return ''


class ExperimentAdmin(admin.ModelAdmin):
    inlines = [VariantInline]
    list_display = ('name', 'info', 'goal_pies')
    list_filter = ('state',)
    fieldsets = (
        (u'Эксперимент', {'fields': ('name', 'description', 'start_date', 'state', 'goals',)}),
        (u'Ежедневная статистика', {
            'fields': ('daily_stat',),
            'classes': ('collapse',)
        }),
    )
    readonly_fields = ('start_date', 'daily_stat')

    class Media:
        js = (
            'https://d3js.org/d3.v3.min.js',
            'https://cdn.rawgit.com/novus/nvd3/v1.8.1/build/nv.d3.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js',
            settings.STATIC_URL + 'js/experiments.admin_inline.js',
            settings.STATIC_URL + 'js/experiments.admin_piecharts.js',
            settings.STATIC_URL + 'js/experiments.admin_dailystat.js',
        )
        css = {
            'all': (
                settings.STATIC_URL + 'css/experiments.admin.css',
                'https://cdn.rawgit.com/novus/nvd3/v1.8.1/build/nv.d3.css',
            )
        }

    @admin_column(u'Цели')
    def goal_pies(self, obj):
        stat = self.get_experiment_stat(obj.name)
        for goal_name, goal in stat.goals.iteritems():
            goal_variants = goal.variants.items()
            goal.pies_data = json.dumps([
                {'label': u"{} ({})".format(variant_name, variant.hits), 'value': variant.conversion * 100}
                for variant_name, variant in goal_variants
            ])

            # Ссылка на калькулятор ABBA
            goal.abba_link = (
                u'http://thumbtack.github.io/abba/demo/abba.html#' +
                u'&'.join((u"{variant}={successes}%2C{trials}".format(
                    variant=variant_name,
                    successes=goal_variant.hits,
                    trials=goal_variant.variant.participants,
                )) for variant_name, goal_variant in goal_variants) +
                u'&abba%3AintervalConfidenceLevel=0.95&abba%3AuseMultipleTestCorrection=true'
            )

            # Ссылка на калькулятор размера выборки
            first_variant_conversion = goal_variants[0][1].conversion
            best_conversion = max(goal_variants, key=lambda (name, variant): variant.conversion)[1].conversion
            if first_variant_conversion and best_conversion > first_variant_conversion:
                effect = (best_conversion / first_variant_conversion - 1)
                goal.sample_size_link = (
                    u'https://www.optimizely.com/sample-size-calculator/?' +
                    u'conversion={conversion:.2f}&effect={effect:.2f}&significance=95'.format(
                        conversion=first_variant_conversion * 100,
                        effect=effect * 100,
                    )
                )

        return mark_safe(render_to_string('admin/goals_pies.html', {
            'experiment': obj.name,
            'stat': stat
        }))

    @admin_column(u'Информация')
    def info(self, obj):
        stat  = self.get_experiment_stat(obj.name)
        return mark_safe(render_to_string('admin/experiment_info.html', {
            'experiment': obj,
            'stat': stat
        }))

    @admin_column(u'Ежедневная статистика')
    def daily_stat(self, obj):
        stat = self.get_experiment_daily_stat(obj.name)

        for goal, goal_stat in stat.iteritems():
            variants_stat = defaultdict(list)
            for date in sorted(goal_stat.keys()):
                date_timestamp = to_timestamp(datetime.datetime.combine(date, datetime.time()))
                for variant, conversion in goal_stat[date].iteritems():
                    variants_stat[variant].append((date_timestamp, conversion))

            stat[goal] = json.dumps([
                {'key': variant, 'values': variant_stat}
                for variant, variant_stat in variants_stat.iteritems()
            ])

        return mark_safe(render_to_string('admin/daily_stat.html', {
            'experiment': obj,
            'stat': stat
        }))


    def get_queryset(self, *args, **kwargs):
        # Сброс инстанс-кеша статистики экспериментов перед получением объекта или списка объектов
        self.get_experiment_stat.clear_obj_cache()
        self.get_experiment_daily_stat.clear_obj_cache()
        return super(ExperimentAdmin, self).get_queryset(*args, **kwargs)

    @memoized_in_object
    def get_experiment_stat(self, name):
        return get_experiment_stat(name, refresh_cache_on_miss=True)

    @memoized_in_object
    def get_experiment_daily_stat(self, name):
        return get_experiment_daily_stat(name, refresh_cache_on_miss=True)


admin.site.register(ExperimentModel, ExperimentAdmin)
