# coding: utf-8
from cache.decorators import cached  # Декоратор из основного проекта
from common.redis import get_redis_connection  # из основного проекта

from .consts import ExperimentState
from .experiment import Experiment
from .models import ExperimentModel
from .tracker import RedisTracker


@cached(key='get_all_experiments')
def get_all_experiments():
    """
    Возвращает словарь всех экспериментов по имени
    Данные кешируются в памяти и сбрасываются при изменении экспериментов в БД
    :type: dict[str | unicode, Experiment]
    """
    experiments = {}
    for experiment in ExperimentModel.objects.defer('description').prefetch_related('variants'):
        experiments[experiment.name] = Experiment(
            name=experiment.name,
            state=experiment.state,
            variants=experiment.variants_list,
            goals=experiment.goals_list,
            default_variant=experiment.default_variant
        )
    return experiments


def get_experiment(name, refresh_cache_on_miss=False):
    """
    Возвращает данные эксперимента по названию из кеша
    :param str | unicode name: эксперимент
    :param bool refresh_cache_on_miss: сбросить кеш если эесперимент не нашёлся в кеше
    :rtype: Experiment
    """
    experiment = get_all_experiments().get(name)
    if not experiment and refresh_cache_on_miss:
        get_all_experiments.clear_cache()
        experiment = get_all_experiments().get(name)

    return experiment


def get_experiment_stat(experiment_name, refresh_cache_on_miss=False):
    """
    Возвращает статистику по эксперименту
    :param str | unicode experiment_name: эксперимент
    :param bool refresh_cache_on_miss: сбросить кеш если эесперимент не нашёлся в кеше
    :rtype: experiments.statistics.ExperimentStat
    """
    experiment = get_experiment(experiment_name, refresh_cache_on_miss)
    tracker = RedisTracker(get_redis_connection())
    return tracker.get_experiment_stat(experiment.name, experiment.variants, experiment.goals)


def get_experiment_daily_stat(experiment_name, refresh_cache_on_miss=False):
    """
    Возвращает ежедневнуб статистику по эксперименту
    :param str | unicode experiment_name: эксперимент
    :param bool refresh_cache_on_miss: сбросить кеш если эесперимент не нашёлся в кеше
    :rtype: dict[str, dict[datetime.date, dict[str, float]]]
    """
    experiment = get_experiment(experiment_name, refresh_cache_on_miss)
    tracker = RedisTracker(get_redis_connection())
    print "get_experiment_daily_stat", experiment_name
    return tracker.get_experiment_daily_stat(experiment.name, experiment.variants, experiment.goals)


def get_active_experiment_names():
    """
    Возвращает имена всех актуальных экспериментов (активных и на стадии завершения)
    :rtype: set
    """
    return {
        experiment_name
        for experiment_name, experiment in get_all_experiments().iteritems()
        if experiment.state in {ExperimentState.ACTIVE, ExperimentState.COMPLETION}
    }
