# coding: utf-8
import robot_detection
from cache.decorators import lazy_property  # декоратор "ленивых" свойств из основного проекта

from .base import BaseParticipant, ParticipantExperimentData


class SessionParticipant(BaseParticipant):
    """
    Интерфейс участника эксперимента на сессиях
    """
    SESSION_KEY = 'experiments'
    IS_HUMAN_KEY = 'is_human'

    def __init__(self, session, user_agent=None, active_experiments=None):
        """
        :param django.contrib.sessions.backends.base.SessionBase session: сессия
        :param str|None user_agent: user-agent пользователя для проверки на человечность
        :param set[str] | None active_experiments: имена актуальных экспериментов (для очистки устаревших)
        """
        self.session = session
        self.user_agent = user_agent
        self.active_experiments = active_experiments or set()
        self.cleaned = False

    def get_experiment_data(self, experiment):
        data = self.session.get(self.SESSION_KEY, {}).get(experiment, {})
        return ParticipantExperimentData(
            variant=data.get('variant'),
            goals=data.get('goals'),
        )

    def set_experiment_data(self, experiment, data):
        experiments = self.session.get(self.SESSION_KEY)
        if experiments is None:
            experiments = self.session[self.SESSION_KEY] = {}
        else:
            # Если у участника уже имеются записи об эксперимнтах очищаем устаревшие
            self.cleanup_inactive(experiments)

        experiments[experiment] = {
            'variant': data.variant,
            'goals': sorted(data.goals),
        }

        self.session.modified = True

    @property
    def is_human(self):
        """Людьми считаем неботов прошедших проверку яваскриптом"""
        return not self.is_bot and self.session.get(self.IS_HUMAN_KEY, False)

    def confirm_human(self):
        self.session[self.IS_HUMAN_KEY] = True

    @lazy_property
    def is_bot(self):
        """Пользователя без user_agent считаем ботом"""
        return robot_detection.is_robot(self.user_agent) if self.user_agent else True

    def cleanup_inactive(self, experiments):
        """
        Очищает записи более не ктуальных экспериментов,
        """
        if not self.cleaned:
            for experiment_name in experiments.keys():
                if experiment_name not in self.active_experiments:
                    del experiments[experiment_name]
                    self.session.modified = True

        self.cleaned = True
