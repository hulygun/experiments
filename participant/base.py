# coding: utf-8
class ParticipantExperimentData(object):
    def __init__(self, variant=None, goals=None):
        self.variant = variant
        self.goals = set(goals or [])


class BaseParticipant(object):
    def get_experiment_data(self, experiment):
        """
        Получение данных об эксперименте
        :param experiment:
        :return: ParticipantExperimentData instance
        """
        raise NotImplementedError()  # pragma: no cover

    def set_experiment_data(self, experiment, data):
        """
        Установка значений данных об эксперименте
        :param experiment:
        :param data:
        :return:
        """
        raise NotImplementedError()  # pragma: no cover

    def get_variant(self, experiment):
        """
        Получаем вариант эксперемента по имени для текущей сессии
        :param experiment: str Название эксперимента
        :return: mixed(str|None) Значение варианта или None
        """
        return self.get_experiment_data(experiment).variant

    def set_variant(self, experiment, variant):
        """
        Устанавливает значение варианта для эксперимента
        :param experiment: str Название эксперимента
        :param variant: str Вариант
        """
        data = self.get_experiment_data(experiment)
        if variant != data.variant:
            data.variant = variant
            data.goals = []
            self.set_experiment_data(experiment, data)

    def is_goal_reached(self, experiment, goal):
        """
        Проверка достигнута ли цель
        :param experiment: str Название эксперимента
        :param goal: str Цель эксперимента
        :rtype: bool
        """
        return goal in self.get_experiment_data(experiment).goals

    def set_goal_reached(self, experiment, goal):
        """
        Установка цели для эксперимента текущей сессии
        :param experiment: str Название эксперимента
        :param goal: str Цель
        """
        data = self.get_experiment_data(experiment)
        if goal not in data.goals:
            data.goals.add(goal)
            self.set_experiment_data(experiment, data)

    @property
    def is_human(self):
        raise NotImplementedError()  # pragma: no cover

    def confirm_human(self):
        raise NotImplementedError()  # pragma: no cover
