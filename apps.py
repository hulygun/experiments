# coding: utf-8
from django.apps import AppConfig


class ExperimentConfig(AppConfig):
    name = 'experiments'
    verbose_name = u'_Эксперименты'

    def ready(self):
        from . import signals
