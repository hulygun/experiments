# coding: utf-8
from collections import namedtuple
import logging
import random

from .exceptions import WrongGoal, WrongVariant
from .consts import ExperimentState

logger = logging.getLogger('experiments.manager')
VariantResult = namedtuple('Variant', ['name', 'selected', 'is_default'])
GoalResult = namedtuple('Goal', ['was_reached', 'is_first_time'])


class ExperimentManager(object):
    """
    Управляющий класс экспериментами
    """
    def __init__(self, experiment, participant, tracker):
        """
        :param experiments.experiment.Experiment experiment: эксперимент
        :param experiments.participant.base.BaseParticipant participant: участник эксперимента
        :param experiments.tracker.base.BaseTracker tracker: трекер экспермента
        """
        self.experiment = experiment
        self.participant = participant
        self.tracker = tracker

    @property
    def name(self):
        return self.experiment.name

    @property
    def variant(self):
        return self.participant.get_variant(self.experiment.name)

    @property
    def default_variant(self):
        return self.experiment.default_variant

    def get_or_select_variant(self):
        """
        Возвращает вариант для текущего участника эксперимента
        назначая случайный, если он ещё не был установлен
        :rtype: str, bool, bool
        :return: variant, selected, is_default (вариант, выбран новый, возвращён дефолтный)
        """
        if self.participant.is_bot:  # Участник эксперимента известный бот
            return VariantResult(self.default_variant, False, True)

        variant, selected, is_default = self.variant, False, False

        if self.experiment.state == ExperimentState.ACTIVE:  # Если эксперимент активен
            # Назначаем новый вариант, если ещё не назначен
            if not variant:
                variant, selected = self._select_variant(), True

        elif self.experiment.state == ExperimentState.COMPLETION:  # Эксперимент на стадии завершения
            # Если вариант ещё не назначен, возвращаем дефолтный
            if not variant:
                variant, is_default = self.default_variant, True

        elif self.experiment.state == ExperimentState.INACTIVE:  # Эксперимент неактивен
            # Возвращаем дефолтный вариант
            variant, is_default = self.default_variant, True

        return VariantResult(variant, selected, is_default)

    def goal(self, goal):
        """
        Фиксирует достижение цели эксперимента
        :param str goal: цель
        :rtype: bool
        :return was_reached, is_first_time (цель достигнута, достигнута впервые)
        :raises: WrongGoal
        """
        if goal not in self.experiment.goals:  # Цель не описана в настройках эксперимента
            raise WrongGoal(goal, self.experiment.goals)

        if self.experiment.state != ExperimentState.INACTIVE:  # Эксперимент активен или завершается
            if not self.participant.is_human:
                # Участник не человек
                logger.info('Participant is not human. Goal "{}" not reached'.format(goal))
            elif self.variant:
                return GoalResult(True, self._set_goal(goal))
            else:
                # Достигнута цель, но вариант не был назначен
                logger.info('Goal "{}" reached, but no variant selected for participant'.format(goal))

        return GoalResult(False, False)

    def set_variant(self, variant):
        """
        Принудительная установка варианта без случайного выбора (для тестов)
        :param str variant: вариант
        :raises: WrongVariant
        """
        if variant not in self.experiment.variants:
            raise WrongVariant(variant, self.experiment.variants)

        if self.experiment.state == ExperimentState.ACTIVE:
            self._set_variant(variant)

    def _select_variant(self):
        """
        Выбирает случайный вариант и устанавливает его для текущего участника эксперимента
        :rtype: str
        """
        variant = random.choice(self.experiment.variants)
        self._set_variant(variant)
        return variant

    def _set_variant(self, variant):
        """
        Устанавливает вариант для текущего участника эксперимента и трекает участие
        :param str variant: вариант
        """
        self.participant.set_variant(self.experiment.name, variant)
        self.tracker.track_participant(self.experiment.name, variant)

    def _set_goal(self, goal):
        """
        Устанавливает достижение цели для текущего участника эксперимента и трекает это событие
        :param str goal: цель
        :rtype: bool
        :return True если цель была достигнута впервые, False если повторно
        """
        # Если цель уже была достигнута ничего не делаем и возвращаем False
        if self.participant.is_goal_reached(self.experiment.name, goal):
            return False

        self.participant.set_goal_reached(self.experiment.name, goal)
        self.tracker.track_goal(self.experiment.name, self.variant, goal)
        return True
