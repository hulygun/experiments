# coding: utf-8
from django.conf import settings
from django.utils.deprecation import MiddlewareMixin

from common.redis import get_redis_connection  # из основного проекта

from .cache_helpers import get_experiment, get_active_experiment_names
from .manager import ExperimentManager
from .participant import SessionParticipant
from .tracker import RedisTracker, DummyTracker


class ExperimentsMiddleware(MiddlewareMixin):
    """
    Добавляет в request:
     - метод `.experiments` (возвращает менеджер эксперимента по имени для текущего пользователя)
     - свойство `.participant` (объект для управления участником эксперимента)
    """
    IGNORE_URLS = {'/yml/', '/partner/', '/ses_logs/', '/healthcheck', '/callback/create_receipt/'}

    def process_request(self, request):
        if self.is_suitable_request(request):
            self.add_experiments_to_request(request)

            if 'set_variant' in request.GET:
                self.set_experiment_variant(request)

    def add_experiments_to_request(self, request):
        # Участник эксперимента
        participant = SessionParticipant(
            session=request.session,
            user_agent=request.META.get('HTTP_USER_AGENT'),
            active_experiments=get_active_experiment_names()
        )

        # Трекер статистики эксперимента
        if not request.user.is_anonymous and (request.user.is_superuser or request.user.is_staff) and not settings.DEBUG:
            tracker = DummyTracker()
        else:
            tracker = RedisTracker(get_redis_connection())

        # Менеджер экспериментов
        experiments_cache = {}
        request.experiments = self.get_experiment_manager_factory(participant, tracker, experiments_cache)
        request.participant = participant

    def set_experiment_variant(self, request):
        """
        Принудительно устанавливает текущему участнику эксперимента указанный вариант в эксперименте
        если URL содержит GET-параметр `set_variant=experiment:variant`
        """
        try:
            experiment_name, variant = request.GET['set_variant'].split(':')
        except ValueError:
            return

        experiment = request.experiments(experiment_name)
        if experiment:
            experiment.set_variant(variant)

    @classmethod
    def get_experiment_manager_factory(cls, participant, tracker, cache):
        def experiment_manager_factory(name):
            """
            Возвращает менеджер указанного эксперимента сконфигурированный для текущего пользователя
            :param str | unicode name: название эксперимента
            :rtype: ExperimentManager
            """
            if name not in cache:  # cache — локальный кэш на один request
                experiment = get_experiment(name)
                if experiment:
                    cache[name] = ExperimentManager(experiment, participant, tracker)
                else:
                    cache[name] = None

            return cache[name]

        return experiment_manager_factory

    def is_suitable_request(self, request):
        for prefix in self.IGNORE_URLS:
            if request.path.startswith(prefix):
                return False

        return True
