# coding: utf-8
class WrongGoal(Exception):
    def __init__(self, goal, available_goals):
        goals = u",".join(available_goals)
        super(WrongGoal, self).__init__(u'"{}" not in availables goals: {}'.format(goal, goals))


class WrongVariant(Exception):
    def __init__(self, variant, available_variants):
        variants = u",".join(available_variants)
        super(WrongVariant, self).__init__(u'"{}" not in availables variants: {}'.format(variant, variants))
