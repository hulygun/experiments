# coding: utf-8
class Experiment(object):
    """
    DTO-объект для хранения настроек эксперимента
    В этом виде они кешируются, возвращаются `get_experiment` и принимаются `ExperimentManager`
    """
    def __init__(self, name, state, variants, goals, default_variant):
        """
        :param str | unicode name: название эксперимента
        :param int state: состояние эесперимента (из consts.ExperimentsState)
        :param list[str] variants: список возможных вариантов
        :param list[str] goals: список доступных целей
        :param str default_variant: деволтный вариант
        """
        self.name = name
        self.state = state
        self.variants = variants
        self.goals = goals
        self.default_variant = default_variant
