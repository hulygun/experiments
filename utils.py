# coding: utf-8
from common.redis import get_redis_connection  # из основного проекта

from .tracker import RedisTracker


def save_experiment_daily_stats(experiment_name, stat):
    """
    Сохраняет ежедневневный слепок статистики по эксперименту
    :param str | unicode experiment_name: эксперимент
    :param experiments.statistics.ExperimentStat stat: статистика
    """
    tracker = RedisTracker(get_redis_connection())
    for variant_name, variant in stat.variants.iteritems():
        for goal_name, goal in variant.goals.iteritems():
            tracker.track_daily_stats(experiment_name, variant_name, goal_name, goal.conversion)
