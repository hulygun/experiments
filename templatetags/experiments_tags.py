# coding: utf-8
import inspect

from django import template
from django.template import loader
from django.template.library import parse_bits

register = template.Library()


@register.tag
def experiment(parser, token):
    """
    Условный вывод блока в зависимости от назначенного в эксперименте варианта
    При отсутствии варианта назвачает его.

    Пример::

      {% experiment "experiment_name" "variant_name" %}
        Код для участников эксперимента "experiment_name"
        с выбранным вариантом "variant_name"
      {% endexperiment %}
    """
    content = parser.parse(('endexperiment',))
    parser.delete_first_token()
    bits = token.split_contents()[1:]
    args, kwargs = parse_tag_args('experiment', parser, bits, ExperimentNode.__init__, {'content'})
    return ExperimentNode(content, *args, **kwargs)


@register.tag
def experiment_goal(parser, token):
    """
    Фиксация достижения цели `goal` для участника эксперимента `experiment`.

    Пример::

      {% experiment_goal "experiment" "goal" %}
        Опционально: код который бвыполнится в случае успешнго достижения цели
        {% if is_first_time %}
            <script>GAExperimentTracker.trackGoal('{{ experiment }}', '{{ variant }}', '{{ goal }}');</script>
        {% endif %}
      {% endexperiment_goal %}
    """
    content = parser.parse(('endexperiment_goal',))
    parser.delete_first_token()
    bits = token.split_contents()[1:]
    args, kwargs = parse_tag_args('endexperiment_goal', parser, bits, ExperimentGoalNode.__init__, {'content'})
    return ExperimentGoalNode(content, *args, **kwargs)


class ExperimentNode(template.Node):
    def __init__(self, content, experiment, variant, is_default=False):
        self.content = content
        self.experiment = experiment
        self.variant = variant
        self.is_default = is_default or template.Variable('False')

    def render(self, context):
        experiment_name = self.experiment.resolve(context)
        block_variant = self.variant.resolve(context)
        is_default = self.is_default.resolve(context)

        experiment = context.request.experiments(experiment_name)
        if not experiment:
            # Рендерим блок если эксперимент не найден, но у блока стоит флаг is_default
            return self.content.render(context) if is_default else ''

        variant = experiment.get_or_select_variant()
        js_add_variant = render_js_add_variant(experiment_name, variant.name, variant.selected, variant.is_default)
        if variant.name == block_variant:
            # Рендерим блок если вариант совпадает с выбранным у участника эксперимента
            # или с дефолтным при неактивном эксперименте
            return js_add_variant + self.content.render(context)
        else:
            return js_add_variant


class ExperimentGoalNode(template.Node):
    def __init__(self, content, experiment, goal):
        self.content = content
        self.experiment = experiment
        self.goal = goal

    def render(self, context):
        experiment_name = self.experiment.resolve(context)
        goal = self.goal.resolve(context)
        experiment = context.request.experiments(experiment_name)
        if not experiment:  # Эксперимент не определён в базе
            return ''

        # При WrongGoal на существующем эксперименте получим ошибку как задумано
        goal_data = experiment.goal(goal)
        if goal_data.was_reached:
            context.update({
                'experiment': experiment.name,
                'variant': experiment.variant,
                'goal': goal,
                'is_first_time': goal_data.is_first_time
            })
            content = render_js_add_variant(experiment_name, experiment.variant) + self.content.render(context)
            context.pop()
            return content
        else:
            # По какой-то причине цель не была достигнута (эксперимент не активен, ещё не назначен вариант и т.д.)
            return ''


def render_js_add_variant(experiment_name, variant, selected=False, is_default=False):
    """
    Рендерит скрипт сохраняющий выбранный вариант в глобальной переменной
    для доступа через js без лишнего запроса серверу
    """
    return loader.render_to_string('js_add_variant.html', {
        'experiment': experiment_name,
        'variant': variant,
        'selected': selected,
        'is_default': is_default,
    })


def parse_tag_args(name, parser, bits, func, exclude_args=None):
    params, varargs, varkw, defaults = inspect.getargspec(func)
    exclude_args = (exclude_args or set()) | {'self'}
    params = filter(lambda param: param not in exclude_args, params)
    args, kwargs = parse_bits(parser, bits, params, varargs, varkw, defaults, False, name)
    return args, kwargs
