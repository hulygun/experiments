# coding: utf-8
from collections import OrderedDict


class ExperimentStat(object):
    """
    Вспомогательный класс для представления статистики АБ-теста
    >>> stat = ExperimentStat()
    ... stat.set_variant_participants('variant1', 10)
    ... stat.set_variant_participants('variant2', 15)
    ... stat.set_goal_hits('variant1', 'goal1', 5)
    ... stat.set_goal_hits('variant2', 'goal1', 6)
    ... stat.goals['goal1'].hits
    11
    >>> stat.variants['variant1'].goals['goal1'].conversion
    0.5
    >>> stat.variants['variant2'].goals['goal1'].conversion
    0.4
    >>> stat.goals['goal1'].variants['variant2'].conversion
    0.4
    >>> stat.participants
    25
    >>> for variant_name, variant in stat.variant.iteritems():
    ...    for goal_name, goal in variant.goals.iteritems():
    ...        print variant_name, goal_name, goal.conversion
    variant1 goal1 0.5
    variant2 goal1 0.4
    """
    class Variant(object):
        def __init__(self):
            self.participants = 0
            self.goals = OrderedDict()

        def set_goal_hist(self, goal, hits):
            variant_goal = self.goals.setdefault(goal.name, ExperimentStat.VariantGoal(self, goal))
            goal.hits += hits - variant_goal.hits
            variant_goal.hits = hits

    class Goal(object):
        def __init__(self, name, variants):
            self.name = name
            self._variants = variants
            self.hits = 0

        @property
        def variants(self):
            return OrderedDict(
                (variant_name, variant.goals[self.name])
                for variant_name, variant in self._variants.iteritems()
            )

    class VariantGoal(object):
        def __init__(self, variant, goal):
            self.variant = variant
            self.goal = goal
            self.hits = 0

        @property
        def conversion(self):
            return float(self.hits) / self.variant.participants if self.variant.participants else 0.0

    def __init__(self):
        self.variants = OrderedDict()
        self.goals = OrderedDict()
        self.participants = 0

    def set_variant_participants(self, variant_name, participants):
        variant = self.variants.setdefault(variant_name, self.Variant())
        self.participants += participants - variant.participants
        variant.participants = participants

    def set_goal_hits(self, variant_name, goal_name, hits):
        variant = self.variants.setdefault(variant_name, self.Variant())
        goal = self.goals.setdefault(goal_name, self.Goal(goal_name, self.variants))
        variant.set_goal_hist(goal, hits)
