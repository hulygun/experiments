# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ExperimentModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=50, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u044d\u043a\u0441\u043f\u0435\u0440\u0438\u043c\u0435\u043d\u0442\u0430', db_index=True)),
                ('description', models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('state', models.SmallIntegerField(default=0, verbose_name='\u0421\u043e\u0441\u0442\u043e\u044f\u043d\u0438\u0435', choices=[(0, '\u041d\u0435\u0430\u043a\u0442\u0438\u0432\u043d\u044b\u0439'), (1, '\u0410\u043a\u0442\u0438\u0432\u043d\u044b\u0439'), (2, '\u0417\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u0438\u0435')])),
                ('start_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('goals', models.CharField(help_text='\u0421\u043f\u0438\u0441\u043e\u043a \u0446\u0435\u043b\u0435\u0439 \u044d\u043a\u0441\u043f\u0435\u0440\u0438\u043c\u0435\u043d\u0442\u0430 \u0447\u0435\u0440\u0435\u0437 \u0437\u0430\u043f\u044f\u0442\u0443\u044e', max_length=255, verbose_name='\u0426\u0435\u043b\u0438')),
            ],
            options={
                'db_table': 'experiments',
                'verbose_name': '\u042d\u043a\u0441\u043f\u0435\u0440\u0438\u043c\u0435\u043d\u0442',
                'verbose_name_plural': '\u042d\u043a\u0441\u043f\u0435\u0440\u0438\u043c\u0435\u043d\u0442\u044b',
            },
        ),
        migrations.CreateModel(
            name='Variant',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('is_default', models.BooleanField(default=False, verbose_name='\u041f\u043e \u0443\u043c\u043e\u043b\u0447\u0430\u043d\u0438\u044e')),
                ('experiment', models.ForeignKey(related_name='variants', to='experiments.ExperimentModel')),
            ],
            options={
                'db_table': 'experiment_variants',
                'verbose_name': '\u0412\u0430\u0440\u0438\u0430\u043d\u0442',
                'verbose_name_plural': '\u0412\u0430\u0440\u0438\u0430\u043d\u0442\u044b',
            },
        ),
    ]
