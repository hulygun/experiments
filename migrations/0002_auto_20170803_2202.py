# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('experiments', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='experimentmodel',
            name='state',
            field=models.SmallIntegerField(default=0, verbose_name='\u0421\u043e\u0441\u0442\u043e\u044f\u043d\u0438\u0435', choices=[(0, '\u041d\u0435\u0430\u043a\u0442\u0438\u0432\u043d\u044b\u0439'), (1, '\u0410\u043a\u0442\u0438\u0432\u043d\u044b\u0439'), (2, '\u0417\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u0438\u0435 (\u0442\u043e\u043b\u044c\u043a\u043e \u0446\u0435\u043b\u0438)')]),
        ),
        migrations.AlterField(
            model_name='variant',
            name='is_default',
            field=models.BooleanField(default=False, verbose_name='\u041f\u043e \u0443\u043c\u043e\u043b\u0447\u0430\u043d\u0438\u044e \u0434\u043b\u044f \u043d\u0435\u0430\u043a\u0442\u0438\u0432\u043d\u044b\u0445'),
        ),
    ]
