/**
 * Created by hulygun on 29.05.17.
 */
$(function () {
    // Редактирование вариантов экспермиента
    initVariantsEditor();
});

// Редактирование вариантов экспермиента
function initVariantsEditor() {
    var $isDefaultCheckboxes = $('[name$="is_default"]'),
        $checked = $isDefaultCheckboxes.filter(':checked');

    // Выбор первого чекбокса, если не иодин не выбран
    if (!$checked.length) $isDefaultCheckboxes.eq(0).prop('checked', true);

    // Переключение чекбоксов как радио-кнопок (вариант по-умолчанию должен быть только один)
    // Запрет выключения последнего чекбокса
    $isDefaultCheckboxes.on('change', function () {
        var $current = $(this);
        if ($current.prop('checked')) {
            $isDefaultCheckboxes.not($current).prop('checked', false);
        } else {
            $current.prop('checked', true);
        }
    });
}

