experiments = experiments || {};

function Experiment(name) {
    return {
        getOrSelectVariant: function () {
            var $deferred = $.Deferred();
            if (typeof experiments[name] !== 'undefined') {
                $deferred.resolve(experiments[name]);
            } else {
                $.get(EXPERIMENTS_URL, {experiment: name}).done(function (variant) {
                    $deferred.resolve(Experiment.updateCache(name, variant));
                }).fail($deferred.reject);
            }

            return $deferred.promise();
        },

        goal: function (goal) {
            return $.post(EXPERIMENTS_URL, {experiment: name, goal: goal});
        }
    }
}

Experiment.updateCache = function(name, variant) {
    // Обновляем кеш сохраняя флаг `selected` если он был true (первое назначение варианта участнику)
    var selectedBefore = !!(experiments[name] && experiments[name].selected);
    experiments[name] = {
        variant: variant.variant,
        selected: variant.selected || selectedBefore,
        is_default: variant.is_default
    };
    return experiments[name];
};


var GAExperimentTracker = {
    /**
     * Трекинг в GA выбора варианта эксперимента
     *
     * Пример использования:
     *  $(function() {
     *      GAExperimentTracker.trackHit('exp_logo_desc', true);
     *  });
     *
     * @param {string} experiment_name название эксперимента
     * @param {boolean} first_time_only трекать только один раз, в момент присвоения варианта пользователю
     */
    trackHit: function (experiment_name, first_time_only) {
        var experiment = new Experiment(experiment_name);
        experiment.getOrSelectVariant().done(function (variant) {
            // Если вместо варианта возвратился дефолтный (эксперимент неактивен или в завершении)
            // Или указано трекать только первое назначение варианта, а он повторный — ничего не делаем
            if (!variant.is_default && !(first_time_only && !variant.selected)) {
                dataLayer.push({
                    event: 'GAEvent',
                    eventCategory: 'experiment:' + experiment_name,
                    eventAction: 'hit:' + variant.variant,
                    eventNonInteraction: 1
                });
            }
        });
    },

    /**
     * Трекинг в GA достижение цели эксперимента
     *
     * Пример использования внутри тега `experiment_goal`:
     *  {% experiment_goal "experiment" "goal" %}
     *      Опционально: код который бвыполнится в случае успешнго достижения цели
     *      {% if is_first_time %}
     *          <script>
     *              GAExperimentTracker.trackGoal('{{ experiment }}', '{{ variant }}', '{{ goal }}');
     *          </script>
     *      {% endif %}
     *  {% endexperiment_goal %}
     *
     * @param {string} experiment_name название эксперимента
     * @param {string} variant назначенный пользователю вариант
     * @param {string} goal название цели
     */
    trackGoal: function (experiment_name, variant, goal) {
        dataLayer.push({
            event: 'GAEvent',
            eventCategory: 'experiment:' + experiment_name,
            eventAction: 'goal:' + variant + ':' + goal,
            eventNonInteraction: 1
        });
    }
};
