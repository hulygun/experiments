$(function () {
    // Круговые графики целей
    initPieCharts();
});


// Круговые графики целей
function initPieCharts() {
    var $pieCharts = $('.js-pie_chart'),
        $columnPieCharts = $('.column-goal_pies'),
        $notPieChartsColumns = $('.results').find('th:not(.column-goal_pies), td:not(.field-goal_pies)'),
        updateCharts = function() {$pieCharts.experimentPieChart('update')};

    // Инициализация графиков, обновление через секунду после рендеринга и при изменении размеров окна
    $pieCharts.experimentPieChart();
    setTimeout(updateCharts, 1000);
    $(window).on('resize', _.debounce(updateCharts, 150));

    // Клик по заголовку колонки с графиками разворачивает графики на ширину всего экрана
    $columnPieCharts.on('click singleTap', function() {
        $notPieChartsColumns.toggle();
        updateCharts();
    });
}

// Плагин для отображение круговых диаграм целей на d3/nvd3
$.fn.experimentPieChart = function (action) {
    return this.each(function() {
        var $element = $(this),
            labelsOutside = $element.width() <= 300;

        if (!action) {
            var data = $element.data('chart');
            nv.addGraph(function () {
                var chart = nv.models.pieChart()
                    .x(function (d) {return d.label})
                    .y(function (d) {return d.value})
                    .margin({top: 30, right: 0, bottom: 20, left: 0})
                    .showLabels(true)
                    .labelsOutside(labelsOutside)
                    .labelThreshold(.05)
                    .labelType("percent")
                    .legendPosition("right");

                d3.select('#' + $element.attr('id') + ' svg')
                    .datum(data)
                    .transition()
                    .duration(350)
                    .call(chart);

                $element.data('chart-model', chart);
            });
        } else if (action === 'update') {
            var chart = $element.data('chart-model');
            if (chart) {
                chart
                    .labelsOutside(labelsOutside)
                    .update();
            }
        }

        return chart;
    });
};
