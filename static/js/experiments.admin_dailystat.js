$(function () {
    // Графики ежедневной статистики экспериментов
    initDailyStatCharts();
});


function initDailyStatCharts() {
    var $goalDailyStat = $('.js-goal_daily_stat'),
        updateCharts = function() {$goalDailyStat.experimentDailyStatChart('update')};

    // Инициализация графиков, обновление через секунду после рендеринга и при изменении размеров окна
    $goalDailyStat.experimentDailyStatChart();
    setTimeout(updateCharts, 1000);
    $(window).on('resize', _.debounce(updateCharts, 150));
    $(document).on('show.fieldset', function(){setTimeout(updateCharts, 100)}); // Обновление графика при раскрытии раздела
}


// Плагин для отображения рафиков ежедневной статистики экспериментов
$.fn.experimentDailyStatChart = function (action) {
    return this.each(function() {
        var $element = $(this),
            width = $element.width(),
            height = Math.round(Math.max(500, width / 1.618));

        if (!action) {
            var data = $element.data('chart');
            nv.addGraph(function () {
                var chart = nv.models.lineChart()
                    .x(function (d) {return d[0]})
                    .y(function (d) {return d[1]})
                    .color(d3.scale.category10().range())
                    .useInteractiveGuideline(true)
                    .height(height);

                chart.xAxis
                    .tickFormat(function(d) {
                        return d3.time.format('%d.%m.%Y')(new Date(d * 1000))
                    })
                    .staggerLabels(true);

                chart.yAxis
                    .tickFormat(d3.format(',.2%'));

                d3.select($element.get(0))
                    .append('svg')
                    .attr('height', height + 'px')
                    .datum(data)
                    .transition()
                    .duration(350)
                    .call(chart);

                $element.data('chart-model', chart);
            });
        } else if (action === 'update') {
            d3.select($element.get(0)).select('svg')
                .attr('height', height + 'px');

            var chart = $element.data('chart-model');
            if (chart) chart
                .height(height)
                .update();
        }

        return chart;
    });
};
