# coding: utf-8
from celery.schedules import crontab
from celery.task import periodic_task

from .utils import save_experiment_daily_stats
from .cache_helpers import get_experiment_stat
from .models import ExperimentModel


@periodic_task(run_every=crontab(minute="57"), ignore_result=True)
def track_daily_stats():
    names = ExperimentModel.objects.exclude(state=ExperimentModel.STATE_INACTIVE).values_list('name', flat=True)
    for name in names:
        stat = get_experiment_stat(name)
        if stat.participants:
            save_experiment_daily_stats(name, stat)

