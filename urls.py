from django.conf.urls import url

from .views import ExperimentView, confirm_human


app_name = 'experiments'
urlpatterns = [
    url(r'^confirm_human/$', confirm_human, name='confirm_human'),
    url(r'^$', ExperimentView.as_view(), name='main'),
]
