# coding: utf-8
import pytest
from mock import Mock

from experiments.consts import ExperimentState
from experiments.manager import ExperimentManager
from experiments.exceptions import WrongVariant, WrongGoal

from .utils import get_fake_experiment, get_test_participant


def test_select_variant():
    """Если вариант ещё не назначен, он назначается и это событие трекается"""
    experiment = get_fake_experiment(state=ExperimentState.ACTIVE)  # Активный экспериент
    participant = get_test_participant()  # Участник с ещё не выбранным вариантом
    tracker = Mock()
    manager = ExperimentManager(experiment, participant, tracker)
    variant, selected, is_default = manager.get_or_select_variant()
    assert variant in experiment.variants  # Выбрался один из существующих вариантов
    assert selected  # Произошёл выбор
    assert not is_default  # Вернулось не дефолтное значение
    # Был вызван трекер с указанием увеличить счётчик участвующих в эскперименте с указанным вариантом
    tracker.track_participant.assert_called_with(experiment.name, variant)


def test_return_selected_variant():
    """Если вариант уже назначен, возвращается он"""
    experiment = get_fake_experiment(state=ExperimentState.ACTIVE, variants=['var1', 'var2'])  # Активный экспериент
    selected_variant = experiment.variants[1]  # Выбранный вариант участника — второй
    participant = get_test_participant(variant=selected_variant)  # Вариант у участника уже выбран
    tracker = Mock()
    manager = ExperimentManager(experiment, participant, tracker)
    variant, selected, is_default = manager.get_or_select_variant()
    assert variant == selected_variant  # Вернулся выбранный вариант
    assert not selected  # Выбора не проихошло
    assert not is_default  # Вернулось не дефолтное значение
    # Трекинг выбора варианта не был осуществлён
    tracker.track_participant.assert_not_called()


def test_bot_returns_default_variant():
    """Для ботов возвращается дефолтный вариант и не происходит трекинга"""
    experiment = get_fake_experiment(state=ExperimentState.ACTIVE)  # Активный экспериент
    participant = get_test_participant(is_bot=True)  # Участник — бот
    tracker = Mock()
    manager = ExperimentManager(experiment, participant, tracker)
    variant, selected, is_default = manager.get_or_select_variant()
    assert variant == experiment.default_variant  # Вернулось не дефолтное значение
    assert not selected  # Выбора не произошло
    assert is_default
    # Трекинг выбора варианта не был осуществлён
    tracker.track_participant.assert_not_called()


def test_inactive_experiment_returns_default_variant():
    """При неактивном эксперименте всегда возвращается дефолтный вариант"""
    experiment = get_fake_experiment(state=ExperimentState.INACTIVE)  # Неактивный экспериент
    participant = get_test_participant()
    tracker = Mock()
    manager = ExperimentManager(experiment, participant, tracker)
    variant, selected, is_default = manager.get_or_select_variant()
    assert variant == experiment.default_variant  # Вернулось не дефолтное значение
    assert not selected  # Выбора не произошло
    assert is_default
    # Трекинг выбора варианта не был осуществлён
    tracker.track_participant.assert_not_called()


def test_inactive_experiment_and_selected_variant():
    """При неактивном эксперименте всегда возвращается дефолтный вариант, даже если у участника есть выбранный"""
    experiment = get_fake_experiment(state=ExperimentState.INACTIVE)  # Неактивный экспериент
    selected_variant = experiment.variants[0]  # Выбранный вариант участника — первый
    participant = get_test_participant(variant=selected_variant)  # Вариант у участника уже выбран
    tracker = Mock()
    manager = ExperimentManager(experiment, participant, tracker)
    variant, selected, is_default = manager.get_or_select_variant()
    assert variant == experiment.default_variant  # Вернулось не дефолтное значение
    assert not selected  # Выбора не произошло
    assert is_default
    # Трекинг выбора варианта не был осуществлён
    tracker.track_participant.assert_not_called()


def test_experiment_in_completion_state_not_select_variant():
    """Если эксперимент на стадии завершения новые варианты не назначаются"""
    experiment = get_fake_experiment(state=ExperimentState.COMPLETION)  # Эксперимент на стадии завершения
    participant = get_test_participant()
    tracker = Mock()
    manager = ExperimentManager(experiment, participant, tracker)
    variant, selected, is_default = manager.get_or_select_variant()
    assert variant == experiment.default_variant  # Вернулось не дефолтное значение
    assert not selected  # Выбора не произошло
    assert is_default
    # Трекинг выбора варианта не был осуществлён
    tracker.track_participant.assert_not_called()


def test_experiment_in_completion_state_returns_selected_variant():
    """Если эксперимент на стадии завершения и у участника уже выбран вариант — возвращается он"""
    experiment = get_fake_experiment(state=ExperimentState.COMPLETION)
    selected_variant = experiment.variants[0]  # Выбранный вариант участника — первый
    participant = get_test_participant(variant=selected_variant)  # Вариант у участника уже выбран
    tracker = Mock()
    manager = ExperimentManager(experiment, participant, tracker)
    variant, selected, is_default = manager.get_or_select_variant()
    assert variant == selected_variant  # Вернулся выбранный вариант
    assert not selected  # Выбора не проихошло
    assert not is_default  # Вернулось не дефолтное значение
    # Трекинг выбора варианта не был осуществлён
    tracker.track_participant.assert_not_called()


def test_goal():
    """Фиксация достижениея цели в активном эксперименте"""
    experiment = get_fake_experiment(state=ExperimentState.ACTIVE)
    selected_variant = experiment.variants[0]  # Выбранный вариант участника — первый
    participant = get_test_participant(variant=selected_variant)  # Вариант у участника уже выбран
    tracker = Mock()
    manager = ExperimentManager(experiment, participant, tracker)
    was_reached, is_first_time = manager.goal(experiment.goals[0])
    assert was_reached  # Цель была достигнута
    assert is_first_time  # Цель была достигнута впервые
    # Был вызван трекер с указанием увеличить счётчик достижений цели в эксперименте с указанным вариантом
    tracker.track_goal.assert_called_with(experiment.name, selected_variant, experiment.goals[0])


def test_repeated_goal():
    """Повторное достижение цели не фиксируется"""
    experiment = get_fake_experiment(state=ExperimentState.ACTIVE)
    selected_variant = experiment.variants[0]  # Выбранный вариант участника — первый
    participant = get_test_participant(variant=selected_variant, goals=experiment.goals)  # Все цели уже достигнуты
    tracker = Mock()
    manager = ExperimentManager(experiment, participant, tracker)
    was_reached, is_first_time = manager.goal(experiment.goals[0])
    assert was_reached  # Цель была достигнута
    assert not is_first_time  # Цель была достигнута впервые
    # Трекинг достижения цели не был осуществлён
    tracker.track_goal.assert_not_called()


def test_goal_before_variant_selected():
    """Достижение цели до выбора варианта"""
    experiment = get_fake_experiment(state=ExperimentState.ACTIVE)
    participant = get_test_participant()  # Участник с ещё не выбранным вариантом
    tracker = Mock()
    manager = ExperimentManager(experiment, participant, tracker)
    was_reached, is_first_time = manager.goal(experiment.goals[0])
    assert not was_reached  # Цель не была достигнута
    assert not is_first_time  # Цель была достигнута впервые
    # Трекинг достижения цели не был осуществлён
    tracker.track_goal.assert_not_called()


def test_goal_in_completion_state():
    """Если эесперимент на стадии завершения — достижение цели фиксируется"""
    experiment = get_fake_experiment(state=ExperimentState.COMPLETION)
    selected_variant = experiment.variants[0]  # Выбранный вариант участника — первый
    participant = get_test_participant(variant=selected_variant)  # Вариант у участника уже выбран
    tracker = Mock()
    manager = ExperimentManager(experiment, participant, tracker)
    was_reached, is_first_time = manager.goal(experiment.goals[0])
    assert was_reached  # Цель была достигнута
    assert is_first_time  # Цель была достигнута впервые
    # Был вызван трекер с указанием увеличить счётчик достижений цели в эксперименте с указанным вариантом
    tracker.track_goal.assert_called_with(experiment.name, selected_variant, experiment.goals[0])


def test_goal_in_inactive_state():
    """Если эесперимент не активен, цель не фиксируется"""
    experiment = get_fake_experiment(state=ExperimentState.INACTIVE)
    selected_variant = experiment.variants[0]  # Выбранный вариант участника — первый
    participant = get_test_participant(variant=selected_variant)  # Вариант у участника уже выбран
    tracker = Mock()
    manager = ExperimentManager(experiment, participant, tracker)
    was_reached, is_first_time = manager.goal(experiment.goals[0])
    assert not was_reached  # Цель не была достигнута
    assert not is_first_time  # Цель была достигнута впервые
    # Трекинг достижения цели не был осуществлён
    tracker.track_goal.assert_not_called()


def test_goal_for_non_human():
    """Если участник не подтвердил свою человечеость, цель не фиксируется"""
    experiment = get_fake_experiment(state=ExperimentState.ACTIVE)
    selected_variant = experiment.variants[0]
    participant = get_test_participant(variant=selected_variant, is_human=False)  # Участник — не человек
    tracker = Mock()
    manager = ExperimentManager(experiment, participant, tracker)
    was_reached, is_first_time = manager.goal(experiment.goals[0])
    assert not was_reached  # Цель не была достигнута
    assert not is_first_time  # Цель была достигнута впервые
    # Трекинг достижения цели не был осуществлён
    tracker.track_goal.assert_not_called()


def test_wrong_goal_name():
    """При неверном указании цели бросается исключение"""
    experiment = get_fake_experiment(state=ExperimentState.ACTIVE)
    participant = get_test_participant()
    tracker = Mock()
    manager = ExperimentManager(experiment, participant, tracker)
    with pytest.raises(WrongGoal):
        manager.goal('wrong_goal')



def test_set_variant():
    """Принудительная установка варианта"""
    experiment = get_fake_experiment(state=ExperimentState.ACTIVE)
    variant = experiment.variants[0]
    participant = get_test_participant()
    tracker = Mock()
    manager = ExperimentManager(experiment, participant, tracker)
    manager.set_variant(variant)
    participant.set_variant.assert_called_with(experiment.name, variant)  # Был вызов установки варианта участнику
    # Был вызван трекер с указанием увеличить счётчик участвующих в эскперименте с указанным вариантом
    tracker.track_participant.assert_called_with(experiment.name, variant)


def test_set_variant_in_inactive_state():
    """Принудительная установка варианта работает только для активных эспериментов"""
    for state in [ExperimentState.COMPLETION, ExperimentState.INACTIVE]:
        experiment = get_fake_experiment(state=state)
        variant = experiment.variants[0]
        participant = get_test_participant()
        tracker = Mock()
        manager = ExperimentManager(experiment, participant, tracker)
        manager.set_variant(variant)
        participant.set_variant.assert_not_called()  # Вызова установки варианта участнику не было
        # Трекинг выбора варианта не был осуществлён
        tracker.track_participant.assert_not_called()


def test_set_variant_wrong_variant_name():
    """При неверном указании варианта бросается исключение"""
    experiment = get_fake_experiment(state=ExperimentState.ACTIVE)
    participant = get_test_participant()
    tracker = Mock()
    manager = ExperimentManager(experiment, participant, tracker)
    with pytest.raises(WrongVariant):
        manager.set_variant('wrong_variant')


def test_manager_properties():
    """Проверяем вспомогтельные свойства менеджера экспериментов"""
    experiment = get_fake_experiment()
    selected_variant = experiment.variants[0]
    participant = get_test_participant(variant=selected_variant)  # Вариант у участника уже выбран
    tracker = Mock()
    manager = ExperimentManager(experiment, participant, tracker)
    assert manager.name == experiment.name
    assert manager.variant == selected_variant
    assert manager.default_variant == experiment.default_variant
