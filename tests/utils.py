# coding: utf-8
from mock import NonCallableMock, Mock

from experiments.consts import ExperimentState


def get_fake_session():
    class FakeSession(dict):
        modified = False
    return FakeSession()


def get_fake_experiment(name=None, variants=None, goals=None, state=ExperimentState.ACTIVE, default_variant=None):
    variants = variants or ['variant1', 'variant2']

    if default_variant is None:  # Если дефолтный вариант не указан, выбираем 'default'
        default_variant = 'default'

    if default_variant not in variants:  # Если дефолтного варианте нет в списке вариантов — добавляем
        variants.append(default_variant)

    experiment = NonCallableMock(
        variants=variants,
        goals=goals or ['goal1', 'goal2'],
        default_variant=default_variant,
        state=state
    )
    experiment.name = name or 'experiment'
    return experiment


def get_test_participant(variant=None, goals=None, is_bot=False, is_human=True):
    goals = goals or {}
    participant = Mock(is_bot=is_bot, is_human=is_human)
    participant.get_variant.return_value = variant
    participant.is_goal_reached = lambda self, goal: goal in goals
    return participant
