# coding: utf-8
from experiments.participant import SessionParticipant

from .utils import get_fake_session


def test_get_variant():
    session = get_fake_session()
    participant = SessionParticipant(session)
    assert participant.get_variant('experiment') is None


def test_set_variant():
    session = get_fake_session()
    participant = SessionParticipant(session)
    participant.set_variant('experiment', 'variant')
    assert participant.get_variant('experiment') == 'variant'

def test_is_goal_reached():
    session = get_fake_session()
    participant = SessionParticipant(session)
    assert not participant.is_goal_reached('experiment', 'goal')


def test_set_goal_reached():
    session = get_fake_session()
    participant = SessionParticipant(session)
    participant.set_goal_reached('experiment', 'goal1')
    participant.set_goal_reached('experiment', 'goal2')
    assert participant.is_goal_reached('experiment', 'goal1')
    assert participant.is_goal_reached('experiment', 'goal2')
    assert not participant.is_goal_reached('experiment', 'goal3')


def test_is_modified():
    session = get_fake_session()
    participant = SessionParticipant(session)

    # Только чтение
    participant.get_variant('experiment')
    participant.is_goal_reached('experiment', 'goal')
    assert not session.modified

    # Установка варианта
    participant.set_variant('experiment', 'variant')
    assert session.modified
    session.modified = False
    participant.set_variant('experiment', 'variant')  # Повторная установка
    assert not session.modified
    participant.set_variant('experiment', 'variant1')  # Смена варианта
    assert session.modified

    # Фиксация достижения цеи
    session.modified = False
    participant.set_goal_reached('experiment', 'goal')
    assert session.modified
    session.modified = False
    participant.set_goal_reached('experiment', 'goal')  # Повторная фиксация
    assert not session.modified
    participant.set_goal_reached('experiment', 'goal1')  # Добавление новой цели
    assert session.modified


def test_confirm_human():
    session = get_fake_session()
    participant = SessionParticipant(session, user_agent='Safari')
    assert not participant.is_human
    participant.confirm_human()

    session = get_fake_session()
    participant = SessionParticipant(session, user_agent='googlebot')
    participant.confirm_human()
    assert not participant.is_human


def test_is_bot():
    session = get_fake_session()
    participant = SessionParticipant(session, user_agent='googlebot')
    assert participant.is_bot


def test_empty_user_agent_is_bot():
    session = get_fake_session()
    participant = SessionParticipant(session, user_agent='')
    assert participant.is_bot


def test_clean_inactive_experiments():
    session = get_fake_session()
    session['experiments'] = {
        'experiment_active': {},
        'experiment_inactive': {},
    }

    participant = SessionParticipant(session, active_experiments={'experiment_active'})
    assert 'experiment_inactive' in session['experiments']
    assert session.modified is False

    participant.set_variant('experiment_active', 'variant')
    assert 'experiment_inactive' not in session['experiments']
    assert session.modified is True
