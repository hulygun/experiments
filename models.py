# coding: utf-8
from django.db import models

from cache.decorators import lazy_property   # из основного проекта

from .consts import ExperimentState


class ExperimentModel(models.Model):
    STATE_INACTIVE = ExperimentState.INACTIVE
    STATE_ACTIVE = ExperimentState.ACTIVE
    STATE_COMPLETION = ExperimentState.COMPLETION
    EXPERIMENT_STATES = (
        (STATE_INACTIVE, u'Неактивный'),
        (STATE_ACTIVE, u'Активный'),
        (STATE_COMPLETION, u'Завершение (только цели)')
    )

    name = models.CharField(u'Название эксперимента', max_length=50, unique=True, db_index=True)
    description = models.TextField(u'Описание', blank=True, null=True)
    state = models.SmallIntegerField(u'Состояние', default=STATE_INACTIVE, choices=EXPERIMENT_STATES)
    start_date = models.DateTimeField(u'Дата создания', auto_now_add=True)
    goals = models.CharField(u'Цели', max_length=255, help_text=u'Список целей эксперимента через запятую')

    class Meta:
        db_table = 'experiments'
        app_label =  'experiments'
        verbose_name = u'Эксперимент'
        verbose_name_plural = u'Эксперименты'

    def __unicode__(self):
        return self.name

    @lazy_property
    def goals_list(self):
        return [x.strip() for x in self.goals.split(',')]

    @lazy_property
    def variants_list(self):
        # Не используем self.variants.values_list('name', flat=True),
        # так как это гарантированный дополнительный запрос, даже при .prefetch_related('variants')
        return [variant.name for variant in self.variants.all()]

    @lazy_property
    def default_variant(self):
        # Не используем self.variants.get(is_default=True),
        # так как это гарантированный дополнительный запрос, даже при .prefetch_related('variants')
        for variant in self.variants.all():
            if variant.is_default:
                return variant.name
        return None


class Variant(models.Model):
    experiment = models.ForeignKey(ExperimentModel, related_name='variants', on_delete=models.CASCADE)
    name = models.CharField(u'Название', max_length=50)
    is_default = models.BooleanField(u'По умолчанию для неактивных', default=False)

    class Meta:
        db_table = 'experiment_variants'
        app_label = 'experiments'
        verbose_name = u'Вариант'
        verbose_name_plural = u'Варианты'

